package felipe.asyncalgorithm.algorithm.networkservice.listeners;

import felipe.asyncalgorithm.algorithm.model.interfaces.IUser;

/**
 * Created by felipe on 08/11/15.
 */
public interface OnNewGameConfirmListener {
    void onNewGameConfirm(IUser me, IUser other);
}
