package felipe.asyncalgorithm.algorithm.networkservice.listeners;

import java.util.List;

import felipe.asyncalgorithm.algorithm.model.interfaces.IUser;

/**
 * Created by felipe on 07/11/15.
 */
public interface OnUsersUpdateListener {
    void onUsersUpdate(List<IUser> users);
}
