package felipe.asyncalgorithm.algorithm.networkservice;

import com.google.gson.Gson;

import felipe.asyncalgorithm.algorithm.BaseAsyncSolver;
import felipe.asyncalgorithm.algorithm.Bundle;
import felipe.asyncalgorithm.algorithm.Node;
import felipe.asyncalgorithm.algorithm.ObjectId;
import felipe.asyncalgorithm.algorithm.listeners.ISolverHelperListener;
import felipe.asyncalgorithm.algorithm.listeners.OnSolverResultBundleListener;
import felipe.asyncalgorithm.algorithm.network.Server;
import felipe.asyncalgorithm.algorithm.network.listeners.OnMessageReceiveListener;
import felipe.asyncalgorithm.algorithm.utils.Log;

/**
 * Created by felipe on 27/11/15.
 */
public class SolverHelper {
    private Boolean free = true;
    private Boolean active = true;
    private Boolean locked = false;

    public static final int SOLVER_TCP_SERVER_PORT = 7695;
    public static final int MESSAGE_GET_HELP = 1000;

    Server server = null;
    BaseAsyncSolver solver = null;
    Node currentNode = null;
    Object syncControl = new Object();
    int iterationCount = 0;

    private ISolverHelperListener listener = null;

    public void setFree(Boolean free) {
        synchronized (free) {
            this.free = free;
        }
    }

    public Boolean isFree() {
        return free && !BaseAsyncSolver.isRunning() && active;
    }

    public void setActive(Boolean active) {
        synchronized (active) {
            this.active = active;
        }
    }

    public Boolean isActive() {
        return active;
    }

    public void setLocked(Boolean locked) {
        synchronized (locked) {
            this.locked = locked;
        }
    }

    public void setSolver(BaseAsyncSolver solver) {
        this.solver = solver;
    }

    public void setListener(ISolverHelperListener listener) {
        this.listener = listener;
    }

    public void start() {
        if(server == null) {
            server = new Server(SOLVER_TCP_SERVER_PORT, 0);

            server.setOnMessageReceiveListener(new OnMessageReceiveListener() {
                @Override
                public String onMessageReceive(int request, String message) {

                    currentNode = new Gson().fromJson(message, Node.class);

                    synchronized (syncControl) {
                        if(!isFree() || solver == null) {
                            currentNode.setDataResult(null);
                            return new Gson().toJson(currentNode);
                        }

                        setFree(false);
                    }

                    setLocked(true);

                    if (listener != null)
                        listener.onSolverStart();
                    final long time = System.currentTimeMillis();

                    // Generating a correct id
                    solver.setObjectId(new ObjectId(currentNode.getId() + 1));

                    solver.setSolverResultBundleListener(new OnSolverResultBundleListener() {
                        @Override
                        public void onSolverResult(Bundle result, int iterationCount) {
                            currentNode.setDataResult(result);
                            setLocked(false);
                            SolverHelper.this.iterationCount = iterationCount;
                        }
                    });

                    solver.exec(currentNode.getDataParams());

                    Log.i2(this, "SolverHelper locked!");
                    while (locked);
                    Log.i2(this, "SolverHelper unlocked!");

                    setFree(true);

                    if (listener != null)
                        listener.onSolverFinish(System.currentTimeMillis() - time, iterationCount);

                    return new Gson().toJson(currentNode);
                }
            });

            server.startTCPServer();
        }
    }
}
