package felipe.asyncalgorithm.algorithm.networkservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import felipe.asyncalgorithm.algorithm.BaseAsyncSolver;
import felipe.asyncalgorithm.algorithm.networkservice.listeners.OnNewGameConfirmListener;
import felipe.asyncalgorithm.algorithm.networkservice.listeners.OnNewGameErrorListener;
import felipe.asyncalgorithm.algorithm.networkservice.listeners.OnNewGameRequestListener;
import felipe.asyncalgorithm.algorithm.networkservice.listeners.OnUsersUpdateListener;
import felipe.asyncalgorithm.algorithm.model.dao.UserDAO;
import felipe.asyncalgorithm.algorithm.model.interfaces.IUser;
import felipe.asyncalgorithm.algorithm.model.objects.User;
import felipe.asyncalgorithm.algorithm.network.Client;
import felipe.asyncalgorithm.algorithm.network.Server;
import felipe.asyncalgorithm.algorithm.network.listeners.OnClientTCPErrorListener;
import felipe.asyncalgorithm.algorithm.network.listeners.OnMessageReceiveListener;
import felipe.asyncalgorithm.algorithm.network.listeners.OnTCPServerClosedListener;
import felipe.asyncalgorithm.algorithm.network.listeners.OnUDPServerClosedListener;
import felipe.asyncalgorithm.algorithm.network.utils.NetworkUtils;
import felipe.asyncalgorithm.algorithm.utils.Log;

/**
 * Created by felipe on 07/11/15.
 */
public class UsersFinder {

    public static final int DELAY_BROADCAST = 1000;
    public static final int INTERVAL_BROADCAST = 5000;
    public static final int INTERVAL_VERIFY_USERS = 1000;

    public static final int USERS_FINDER_TCP_SERVER_PORT = 7685;
    public static final int USERS_FINDER_UDP_SERVER_PORT = 7686;

    public static final int MESSAGE_TAKE_MY_USER_AND_GIVE_ME = 0;
    public static final int MESSAGE_TAKE_MY_USER = 1;
    public static final int MESSAGE_REQUEST_NEW_GAME = 2;
    public static final int MESSAGE_IS_ONLINE = 3;

    private static UsersFinder instance;
    private Client client;
    private Server server;

    private List<IUser> users = null;
    private IUser userToNewGame = null;
    private boolean allowMyselfAsUser = false;

    private Timer timerBroadcast = null;
    private Timer timerVerifyUsers = null;

    private Map<String, Long> timeStampMap = null;

    private List<OnUsersUpdateListener> onUsersUpdateListeners = null;
    private List<OnNewGameConfirmListener> onNewGameConfirmListeners = null;
    private List<OnNewGameRequestListener> onNewGameRequestListeners = null;
    private List<OnNewGameErrorListener> onNewGameErrorListeners = null;

    private Object asyncControl = new Object();

    private UsersFinder() {

        client = Client.getInstance();

        server = new Server(USERS_FINDER_TCP_SERVER_PORT, USERS_FINDER_UDP_SERVER_PORT);

        users = new Vector<>();

        timeStampMap = new HashMap<>();

        onUsersUpdateListeners = new ArrayList<>();
        onNewGameRequestListeners = new ArrayList<>();
        onNewGameConfirmListeners = new ArrayList<>();
        onNewGameErrorListeners = new ArrayList<>();

        server.setOnTCPServerClosedListener(new OnTCPServerClosedListener() {
            @Override
            public void onTCPServerClosed(Server mServer) {

                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {

                        Log.i2(this, "TCP Server has been closed! Reopening in " + INTERVAL_BROADCAST / 1000 + " seconds...");
                        server.startTCPServer();

                    }
                }, INTERVAL_BROADCAST);

            }
        });

        server.setOnUDPServerClosedListener(new OnUDPServerClosedListener() {
            @Override
            public void onUDPServerClosed(Server mServer) {

                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {

                        Log.i2(this, "TCP Server has been closed! Reopening in " + INTERVAL_BROADCAST / 1000 + " seconds...");
                        server.startUDPServer();

                    }
                }, INTERVAL_BROADCAST);
            }
        });

        server.setOnMessageReceiveListener(new OnMessageReceiveListener() {
            @Override
            public String onMessageReceive(int request, String message) {

                String response = "";
                switch (request) {

                    case MESSAGE_TAKE_MY_USER_AND_GIVE_ME:
                        // Saving user
                        IUser newUser = UserDAO.fromJson(message);

                        if(newUser != null) {

                            saveUser(newUser);
                            // Sending user
                            client.sendMessage(MESSAGE_TAKE_MY_USER, getMyUserMessage(), USERS_FINDER_TCP_SERVER_PORT, newUser.getAddress());

                        }
                        break;

                    case MESSAGE_TAKE_MY_USER:
                        IUser newUser2 = UserDAO.fromJson(message);

                        if(newUser2 != null) {

                            saveUser(newUser2);

                        }
                        break;

                    case MESSAGE_REQUEST_NEW_GAME:
                        response = "RESULT_OK";

                        for (OnNewGameRequestListener l : onNewGameRequestListeners) {

                            l.onNewGameRequest(getMyUser(), UserDAO.fromJson(message));

                        }

                        break;

                    case MESSAGE_IS_ONLINE:
                        IUser user = getMyUser();
                        response = UserDAO.toJson(user);
                        break;

                }

                return response;

            }

        });

        client.addOnClientTCPErrorListener(new OnClientTCPErrorListener() {
            @Override
            public void onClientTCPError(int request, String message) {

                switch (request) {

                    case MESSAGE_REQUEST_NEW_GAME:
                        for (OnNewGameErrorListener l : onNewGameErrorListeners) {

                            l.onNewGameError();

                        }
                        break;

                    case MESSAGE_IS_ONLINE:
                        removeUser(UserDAO.fromJson(message));
                        break;

                }

            }

        });

        client.addOnMessageReceiveListener(new OnMessageReceiveListener() {
            @Override
            public String onMessageReceive(int request, String message) {

                String response = "";
                switch (request) {

                    case MESSAGE_TAKE_MY_USER_AND_GIVE_ME:
                        Log.i2(this, "Algorithm error");
                        break;

                    case MESSAGE_REQUEST_NEW_GAME:
                        if(message.equals("RESULT_OK")) {

                            for (OnNewGameConfirmListener l : onNewGameConfirmListeners) {

                                l.onNewGameConfirm(getMyUser(), userToNewGame);

                            }

                        }
                        break;

                    case MESSAGE_IS_ONLINE:
                        IUser user = UserDAO.fromJson(message);

                        if(user != null) {

                            updateUser(user);

                        }

                        break;

                }

                return response;

            }

        });

        startSearch();

    }

    public synchronized static UsersFinder getInstance() {

        if(instance == null) {

            instance = new UsersFinder();

        }

        return instance;

    }

    public void setAllowMyselfAsUser(boolean allowMyselfAsUser) {
        this.allowMyselfAsUser = allowMyselfAsUser;

        if(!allowMyselfAsUser) removeUser(getMyUser());
    }

    public boolean isAllowMyselfAsUser() {
        synchronized (asyncControl) {
            return allowMyselfAsUser;
        }
    }

    public void addOnUsersUpdateListener(OnUsersUpdateListener onUsersUpdateListener) {

        synchronized (asyncControl) {

            this.onUsersUpdateListeners.add(onUsersUpdateListener);

        }

    }

    public void addOnNewGameConfirmListener(OnNewGameConfirmListener onNewGameConfirmListener) {

        synchronized (asyncControl) {

            this.onNewGameConfirmListeners.add(onNewGameConfirmListener);

        }

    }

    public void addOnNewGameRequestListener(OnNewGameRequestListener onNewGameRequestListener) {

        synchronized (asyncControl) {

            this.onNewGameRequestListeners.add(onNewGameRequestListener);

        }

    }

    public void addOnNewGameErrorListener(OnNewGameErrorListener onNewGameErrorListener) {

        synchronized (asyncControl) {

            this.onNewGameErrorListeners.add(onNewGameErrorListener);

        }

    }

    public void startSearch() {

        synchronized (asyncControl) {

            Log.i2(this, "Starting users search...");

            server.startUDPServer();
            server.startTCPServer();

            Log.i2(this, "Starting broadcast timer...");

            timerBroadcast = new Timer();
            timerBroadcast.schedule(new TimerTask() {
                @Override
                public void run() {
                    // Broadcast my user
                    String userMessage = getMyUserMessage();
                    client.sendMessage(MESSAGE_TAKE_MY_USER_AND_GIVE_ME, userMessage, USERS_FINDER_UDP_SERVER_PORT);

                }

            }, DELAY_BROADCAST, INTERVAL_BROADCAST);

            timerVerifyUsers = new Timer();
            timerVerifyUsers.schedule(new TimerTask() {
                @Override
                public void run() {

                    synchronized (asyncControl) {

                        for (IUser u : users) {

                            client.sendMessage(MESSAGE_IS_ONLINE, UserDAO.toJson(u), USERS_FINDER_TCP_SERVER_PORT, u.getAddress());

                        }

                    }

                }
            }, DELAY_BROADCAST, INTERVAL_VERIFY_USERS);

            Log.i2(this, "Finder started!");

        }

    }

    public void stopSearch() {

        synchronized (asyncControl) {

            server.stopTCPServer();
            server.stopUDPServer();

        }

    }

    public void requestNewGame(IUser other) {

        userToNewGame = other;
        client.sendMessage(MESSAGE_REQUEST_NEW_GAME, getMyUserMessage(), USERS_FINDER_TCP_SERVER_PORT, other.getAddress());

    }

    private IUser getMyUser() {

        String myAddress = NetworkUtils.getIpAddress();
        IUser myUser = new User(myAddress, myAddress);
        myUser.setFree(!BaseAsyncSolver.isRunning());
        return myUser;

    }

    private String getMyUserMessage() {

        IUser myUser = getMyUser();
        return UserDAO.toJson(myUser);

    }

    private void saveUser(IUser user) {

        synchronized (asyncControl) {

            if (user != null && user.getName() != null && user.getAddress() != null) {

                if (!allowMyselfAsUser && user.getName().equals(getMyUser().getName())) return;

                timeStampMap.put(user.getName(), System.currentTimeMillis());

                for (IUser u : users) {

                    if (u.getName().equals(user.getName())) {

                        return;

                    }

                }

                users.add(user);
                Log.i2(this, "New user find: " + user.getName());

                List<IUser> mUsers = getUsers();
                for (OnUsersUpdateListener l : onUsersUpdateListeners) {

                    l.onUsersUpdate(mUsers);

                }

            }

        }

    }

    private void removeUser(IUser user) {

        synchronized (asyncControl) {

            int i = 0;
            for (IUser u : users) {

                if (user.getName().equals(u.getName())) {

                    Log.i2(this, "Removing user: " + user.getName());
                    users.remove(i);
                    break;

                }

                i++;

            }

            List<IUser> mUsers = getUsers();
            for (OnUsersUpdateListener l : onUsersUpdateListeners) {

                l.onUsersUpdate(mUsers);

            }

        }

    }

    public List<IUser> getUsers() {

        synchronized (asyncControl) {

            List<IUser> mUsers = new ArrayList<>();
            for (IUser u : users) {

                IUser user = new User(u.getName(), u.getAddress());
                user.setFree(u.isFree());
                mUsers.add(user);

            }

            return mUsers;

        }


    }

    public void updateUser(IUser user) {

        synchronized (asyncControl) {

            for(IUser u: users) {

                if (user.getName().equals(u.getName())) {

                    u.setFree(user.isFree());
                    break;

                }

            }


            List<IUser> mUsers = getUsers();
            for (OnUsersUpdateListener l : onUsersUpdateListeners) {

                l.onUsersUpdate(mUsers);

            }

        }

    }

    public IUser getFreeUser() {

        synchronized (asyncControl) {

            for (IUser u : users) {

                if (u.isFree()) {

                    Log.i2(this, "Free user find: " + u.getName());

                    u.setFree(false);

                    return u;

                }

            }

        }

        return null;

    }

    public void setFreeUser(IUser user) {

        synchronized (asyncControl) {

            for (IUser u : users) {

                if (u.getName().equals(user.getName())) {

                    Log.i2(this, "Setting user free: " + u.getName());
                    u.setFree(true);
                    break;

                }

            }

        }

    }

}
