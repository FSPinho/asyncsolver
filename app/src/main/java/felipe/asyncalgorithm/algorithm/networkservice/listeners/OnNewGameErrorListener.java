package felipe.asyncalgorithm.algorithm.networkservice.listeners;

/**
 * Created by felipe on 08/11/15.
 */
public interface OnNewGameErrorListener {
    void onNewGameError();
}
