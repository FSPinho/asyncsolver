package felipe.asyncalgorithm.algorithm.networkservice;

import felipe.asyncalgorithm.algorithm.Node;
import felipe.asyncalgorithm.algorithm.NodeDAO;
import felipe.asyncalgorithm.algorithm.listeners.OnRemoteSolverResultListener;
import felipe.asyncalgorithm.algorithm.model.interfaces.IUser;
import felipe.asyncalgorithm.algorithm.network.Client;
import felipe.asyncalgorithm.algorithm.network.listeners.OnClientTCPErrorListener;
import felipe.asyncalgorithm.algorithm.network.listeners.OnMessageReceiveListener;
import felipe.asyncalgorithm.algorithm.utils.Log;

public class RemoteSolver {

	private OnRemoteSolverResultListener remoteSolverResultListener = null;

    private final Client client = Client.getNewInstance();
    private final UsersFinder finder = UsersFinder.getInstance();
	
	public void setRemoteSolverResultListener(OnRemoteSolverResultListener remoteSolverResultListener) {

		this.remoteSolverResultListener = remoteSolverResultListener;

	}
	
	public void solveRemote(final Node requestedNode) {

        final IUser user = finder.getFreeUser();

        if(user == null) {

            callError(requestedNode);

        } else {

            client.addOnClientTCPErrorListener(new OnClientTCPErrorListener() {
                @Override
                public void onClientTCPError(int request, String message) {

                    finder.setFreeUser(user);

                    if (request == SolverHelper.MESSAGE_GET_HELP) {

                        Node node = NodeDAO.fromJson(message);

                        if (node != null && node.equals(requestedNode)) {

                            Log.i2(this, "Request with network error, solving myself: " + requestedNode.getId());
                            callError(requestedNode);

                        } else {

                            callError(requestedNode);
                            Log.i2(this, "Remote solver: Algorithm error!");

                        }

                    } else {

                        callError(requestedNode);
                        Log.i2(this, "Remote solver: Algorithm error!");

                    }

                }

            });

            client.addOnMessageReceiveListener(new OnMessageReceiveListener() {
                @Override
                public String onMessageReceive(int request, String message) {

                    finder.setFreeUser(user);

                    if (request == SolverHelper.MESSAGE_GET_HELP) {

                        if (!message.equals("")) {

                            Node node = NodeDAO.fromJson(message);

                            if (node != null && node.equals(requestedNode)) {

                                if (node.getDataResult() == null) {

                                    Log.i2(this, "Request can't help error, solving myself: " + requestedNode.getId());
                                    callError(requestedNode);

                                } else {

                                    Log.i2(this, "Request successfully: " + requestedNode.getId());
                                    requestedNode.setDataResult(node.getDataResult());
                                    callSuccess(requestedNode);

                                }

                            } else {

                                callError(requestedNode);
                                Log.i2(this, "Remote solver: Algorithm error!");

                            }

                        } else {

                            callError(requestedNode);
                            Log.i2(this, "Remote solver: Algorithm error!");

                        }

                    } else {

                        callError(requestedNode);
                        Log.i2(this, "Remote solver: Algorithm error!");

                    }

                    return "";

                }
            });

            Log.i2(this, "Requesting best solve for " + requestedNode.getId());
            client.sendMessage(
                    SolverHelper.MESSAGE_GET_HELP,
                    NodeDAO.toJson(requestedNode),
                    SolverHelper.SOLVER_TCP_SERVER_PORT,
                    user.getAddress()
            );

        }

	}

    private void callError(Node currentNode) {

        if(remoteSolverResultListener != null) {

            remoteSolverResultListener.onRemoteResultError(currentNode);

        } else {

            Log.i2(this, "Remote solver: Algorithm error!");

        }

    }

    private void callSuccess(Node currentNode) {

        if(remoteSolverResultListener != null) {

            remoteSolverResultListener.onRemoteResult(currentNode);

        } else {

            Log.i2(this, "Remote solver: Algorithm error!");

        }

    }


}
