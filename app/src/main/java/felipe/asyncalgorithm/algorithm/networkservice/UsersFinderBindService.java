package felipe.asyncalgorithm.algorithm.networkservice;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import felipe.asyncalgorithm.algorithm.utils.Log;

/**
 * Created by felipe on 13/11/15.
 */
public class UsersFinderBindService extends Service {
    private UsersFinder usersFinder = null;
    private Controller controller = new Controller();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return controller;
    }

    @Override
    public void onCreate() {
        Log.i2(this, "Creating users finder on service...");
        usersFinder = UsersFinder.getInstance();

        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public class Controller extends Binder {
        public UsersFinder getUserService() {
            return usersFinder;
        }
    }
}
