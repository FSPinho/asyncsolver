package felipe.asyncalgorithm.algorithm.networkservice.listeners;

import felipe.asyncalgorithm.algorithm.model.interfaces.IUser;

/**
 * Created by felipe on 08/11/15.
 */
public interface OnNewGameRequestListener {
    void onNewGameRequest(IUser me, IUser other);
}
