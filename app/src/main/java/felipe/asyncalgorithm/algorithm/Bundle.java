package felipe.asyncalgorithm.algorithm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Bundle {
	private int level = 0;
	private int parentLevel = 0;
	private int parentOfParent = 0;
	
	Map<String, Integer> integerMap;
	Map<String, Long> longMap;
	Map<String, Double> doubleMap;
	Map<String, Boolean> booleanMap;
	Map<String, String> stringMap;
	
	Map<String, List<Integer>> listIntegerMap;
	Map<String, List<Long>> listLongMap;
	Map<String, List<Double>> listDoubleMap;
	Map<String, List<Boolean>> listBooleanMap;
	Map<String, List<String>> listStringMap;
	
	public Bundle() {
		integerMap 	= new HashMap<String, Integer>();
		longMap 	= new HashMap<String, Long>();
		doubleMap 	= new HashMap<String, Double>();
		booleanMap 	= new HashMap<String, Boolean>();
		stringMap 	= new HashMap<String, String>();
		
		listIntegerMap 	= new HashMap<String, List<Integer>>();
		listLongMap 	= new HashMap<String, List<Long>>();
		listDoubleMap 	= new HashMap<String, List<Double>>();
		listBooleanMap 	= new HashMap<String, List<Boolean>>();
		listStringMap 	= new HashMap<String, List<String>>();
	}
	
	public void put(String key, Integer value) 	{ integerMap.put(key, value); }
	public void put(String key, Long value) 	{ longMap.put(key, value); }
	public void put(String key, Double value) 	{ doubleMap.put(key, value); }
	public void put(String key, Boolean value) 	{ booleanMap.put(key, value); }
	public void put(String key, String value) 	{ stringMap.put(key, value); }
	
	public void putIntArray		(String key, List<Integer> value) 	{ listIntegerMap.put(key, value); }
	public void putLongArray	(String key, List<Long> value) 		{ listLongMap.put(key, value); }
	public void putDoubleArray	(String key, List<Double> value) 	{ listDoubleMap.put(key, value); }
	public void putBooleanArray	(String key, List<Boolean> value) 	{ listBooleanMap.put(key, value); }
	public void putStringArray	(String key, List<String> value) 	{ listStringMap.put(key, value); }
	
	public Integer 	getInt		(String key) { return integerMap.get(key); }
	public Long 	getLong		(String key) { return longMap.get(key); }
	public Double 	getDouble	(String key) { return doubleMap.get(key); }
	public Boolean 	getBoolean	(String key) { return booleanMap.get(key); }
	public String 	getString	(String key) { return stringMap.get(key); }
	
	public List<Integer> 	getIntArray		(String key) { return listIntegerMap.get(key); }
	public List<Long> 		getLongArray	(String key) { return listLongMap.get(key); }
	public List<Double> 	getDoubleArray	(String key) { return listDoubleMap.get(key); }
	public List<Boolean> 	getBooleanArray	(String key) { return listBooleanMap.get(key); }
	public List<String> 	getStringArray	(String key) { return listStringMap.get(key); }
	
	public void setLevel(int level) { this.level = level; }
	public int getLevel() { return level; }

	public int getParentLevel() { return parentLevel; }
	public void setParentLevel(int parentLevel) { this.parentLevel = parentLevel; }
	
	public void setParentOfParent(int parentOfParent) { this.parentOfParent = parentOfParent; }
	public int getParentOfParent() { return parentOfParent; }
	
}
