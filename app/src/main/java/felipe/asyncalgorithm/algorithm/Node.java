package felipe.asyncalgorithm.algorithm;

import java.util.List;
import java.util.Vector;

public class Node {
	private Integer id = -1;
	private Integer parentId = -1;
	private List<Integer> childrenId = null;
	private Bundle dataParams = null;
	private Bundle dataResult = null;
	private int level = 0;
    private Integer childrenCompleteCount = 0;
	
	public Node(Integer parentId, Bundle data, ObjectId objectId) {
        this.id = objectId.getNewId();
		this.parentId = parentId;
		this.dataParams = data;
		childrenId = new Vector<>();
	}

    public Node(Integer id, Integer parentId, Bundle dataParams, int level) {
        this.id = id;
        this.parentId = parentId;
        this.dataParams = dataParams;
        this.level = level;
        childrenId = new Vector<>();
    }

    public synchronized Integer getId() {
		return id;
	}
	
	public synchronized Integer getParentId() {
		return parentId;
	}
	
	public synchronized List<Integer> getChildrenId() {
		return childrenId;
	}
	
	public synchronized void addChild(Node node) {
		this.childrenId.add(node.getParentId());
	}
	
	public synchronized Bundle getDataParams() {
		return dataParams;
	}
	
	public synchronized Bundle getDataResult() {
		return dataResult;
	}
	
	public synchronized void setDataResult(Bundle dataResult) {
		this.dataResult = dataResult;
	}

	public synchronized Node createChild(Bundle dataParams, ObjectId objectId) {
		Node child = new Node(id, dataParams, objectId);
        child.setLevel(this.level + 1);
		childrenId.add(child.getId());
		return child;
	}

	public synchronized int getLevel() {
		return level;
	}

	public synchronized void setLevel(int level) {
		this.level = level;
	}

    public synchronized void addCompleteChild() {
		synchronized (childrenCompleteCount) {
			childrenCompleteCount = new Integer(childrenCompleteCount.intValue() + 1);
		}
    }

    public synchronized boolean isComplete() {
        return new Integer(childrenId.size()).equals(childrenCompleteCount);
    }

	public synchronized int getChildrenCompleteCount() {
		return childrenCompleteCount;
	}

	public synchronized boolean containsChild(Node node) {
        return childrenId.contains(node.getId());
    }

	@Override
	public synchronized String toString() {
		return "Node{" +
				"id=" + id +
				", parentId=" + parentId +
				", level=" + level +
				", childrenCompleteCount=" + childrenCompleteCount +
				'}';
	}

    @Override
    public synchronized boolean equals(Object o) {
        Node node = (Node) o;
        return id.equals(node.getId()) && parentId.equals(node.getParentId());
    }
}
