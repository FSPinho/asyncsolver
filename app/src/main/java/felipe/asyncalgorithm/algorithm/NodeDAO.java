package felipe.asyncalgorithm.algorithm;

import com.google.gson.Gson;

/**
 * Created by felipe on 06/12/15.
 */
public class NodeDAO {
    public static Node fromJson(String json) {

        try {

            return new Gson().fromJson(json, Node.class);

        } catch (Exception e) {

            return null;

        }

    }

    public static String toJson(Node node) {

        try {

            return new Gson().toJson(node);

        } catch (Exception e) {

            return null;

        }

    }
}
