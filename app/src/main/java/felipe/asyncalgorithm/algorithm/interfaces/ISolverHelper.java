package felipe.asyncalgorithm.algorithm.interfaces;

import felipe.asyncalgorithm.algorithm.listeners.ISolverHelperListener;

/**
 * Created by felipe on 14/11/15.
 */
public interface ISolverHelper {
    void addSolverHelperListener(ISolverHelperListener listener);
}
