package felipe.asyncalgorithm.algorithm.listeners;

import felipe.asyncalgorithm.algorithm.Bundle;

/**
 * Created by felipe on 22/11/15.
 */
public interface OnSolverResultBundleListener {
    void onSolverResult(Bundle result, int iterationCount);
}
