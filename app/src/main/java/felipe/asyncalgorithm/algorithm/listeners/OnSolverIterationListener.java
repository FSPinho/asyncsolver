package felipe.asyncalgorithm.algorithm.listeners;

/**
 * Created by felipe on 28/11/15.
 */
public interface OnSolverIterationListener {
    void onSolverIteration(int iterationCount, int deep);
}
