package felipe.asyncalgorithm.algorithm.listeners;

/**
 * Created by felipe on 14/11/15.
 */
public interface ISolverHelperListener {
    void onSolverStart();
    void onRecussionCalled(int itarationCount, int deep);
    void onSolverFinish(long timeElapsed, int iterationCount);
}
