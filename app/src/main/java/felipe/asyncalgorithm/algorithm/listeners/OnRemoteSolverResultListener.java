package felipe.asyncalgorithm.algorithm.listeners;

import felipe.asyncalgorithm.algorithm.Node;

public interface OnRemoteSolverResultListener {
	void onRemoteResult(Node node);
	void onRemoteResultError(Node node);
}
