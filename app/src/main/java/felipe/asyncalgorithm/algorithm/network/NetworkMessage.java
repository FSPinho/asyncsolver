package felipe.asyncalgorithm.algorithm.network;

import felipe.asyncalgorithm.algorithm.network.interfaces.INetworkMessage;

public class NetworkMessage implements INetworkMessage {
	private int requestCode;
	private String message;
	
	public NetworkMessage(int requestCode, String message) {
		this.requestCode = requestCode;
		this.message = message;
	}

	@Override
	public int getRequestCode() {
		return requestCode;
	}

	@Override
	public String getMessage() {
		return message;
	}
	
}
