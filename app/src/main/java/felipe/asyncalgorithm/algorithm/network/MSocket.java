package felipe.asyncalgorithm.algorithm.network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import felipe.asyncalgorithm.algorithm.network.interfaces.INetworkMessage;

public class MSocket {
	private Socket socket = null;
	private ObjectInputStream in = null;
	private ObjectOutputStream out = null;
	
	public MSocket(Socket socket) {
		this.socket = socket;
	}
		
	private boolean openIn() throws IOException {
		if(socket != null && in == null) {
            in = new ObjectInputStream(socket.getInputStream());
            return true;
		}
		
		return false;
	}
	
	private boolean closeIn() throws IOException {
		if(in != null) {
            in.close();
            return true;
        }
		return false;
	}
	
	private boolean openOut() throws IOException {
		if(socket != null && out == null) {
            out = new ObjectOutputStream(socket.getOutputStream());
            return true;
		}
		
		return false;
	}
	
	private boolean closeOut() throws IOException {
		if(out != null) {
            out.close();
            return true;
        }
		
		return false;
	}
	
	public synchronized boolean openInOut() throws IOException {
		return openIn() && openOut();
	}
	
	public synchronized boolean closeInOut() throws IOException {
		return closeIn() && closeOut();
	}
	
	public synchronized boolean close() throws IOException {
		if(socket != null) {
			socket.close();
			return true;
		}
		
		return false;
	}
	
	public synchronized boolean sendMessage(INetworkMessage msg) throws IOException {
		openOut();
		if(out != null) {
			out.writeUTF(NetworkMessageDAO.toJson(msg));
			out.flush();

			return true;
		}
		
		return false;
	}
	
	public synchronized INetworkMessage getMessage() throws IOException {
		openIn();
		if(in != null) {
			INetworkMessage msg = NetworkMessageDAO.fromJson(in.readUTF());

			return msg;
		}
		
		return null;
	}
	
}
