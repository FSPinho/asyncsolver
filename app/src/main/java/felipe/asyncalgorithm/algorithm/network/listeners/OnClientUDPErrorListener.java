package felipe.asyncalgorithm.algorithm.network.listeners;

/**
 * Created by felipe on 07/11/15.
 */
public interface OnClientUDPErrorListener {
    void onClientUDPError(int request, String message);
}
