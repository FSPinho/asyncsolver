package felipe.asyncalgorithm.algorithm.network;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.StringReader;

import felipe.asyncalgorithm.algorithm.network.interfaces.INetworkMessage;

public class NetworkMessageDAO {
	public static INetworkMessage fromJson(String json) {
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(json));
		reader.setLenient(true);
		return gson.fromJson(reader, NetworkMessage.class);
	}
	
	public static String toJson(INetworkMessage message) {
		return new Gson().toJson(message);
	}
}
