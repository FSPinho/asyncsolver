package felipe.asyncalgorithm.algorithm.network.listeners;

/**
 * Created by felipe on 07/11/15.
 */
public interface OnMessageReceiveListener {
    String onMessageReceive(int request, String message);
}
