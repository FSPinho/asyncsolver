package felipe.asyncalgorithm.algorithm.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;

import felipe.asyncalgorithm.algorithm.network.interfaces.INetworkMessage;
import felipe.asyncalgorithm.algorithm.network.listeners.OnMessageReceiveListener;
import felipe.asyncalgorithm.algorithm.network.listeners.OnMessageSendedListener;
import felipe.asyncalgorithm.algorithm.network.listeners.OnTCPServerClosedListener;
import felipe.asyncalgorithm.algorithm.network.listeners.OnUDPServerClosedListener;
import felipe.asyncalgorithm.algorithm.utils.Log;

public class Server {
	private ServerSocket server = null;
	private ConnectionHandler connectionHandler = null;

	private OnMessageSendedListener onMessageSendedListener = null;
	private OnMessageReceiveListener onMessageReceiveListener = null;
	private OnTCPServerClosedListener onTCPServerClosedListener = null;
	private OnUDPServerClosedListener onUDPServerClosedListener = null;
	private Boolean TCPServerRunning = false;
	private Boolean UDPServerRunning = false;

	private BackGroundTCPServer currentTCPServer = null;
	private BackGroundUDPServer currentUDPServer = null;
	private int UDPServerPort = 0;
    private int TCPServerPort = 0;
	
	public Server(int TCPServerPort, int UDPServerPort) {
        this.TCPServerPort = TCPServerPort;
        this.UDPServerPort = UDPServerPort;
        connectionHandler = new ConnectionHandler();
	}

	public void setOnMessageReceiveListener(OnMessageReceiveListener onMessageReceiveListener) {
		this.onMessageReceiveListener = onMessageReceiveListener;
	}

	public void setOnMessageSendedListener(OnMessageSendedListener onMessageSendedListener) {
        this.onMessageSendedListener = onMessageSendedListener;
	}

	public void setOnTCPServerClosedListener(OnTCPServerClosedListener onTCPServerClosedListener) {
        this.onTCPServerClosedListener = onTCPServerClosedListener;
	}

	public void setOnUDPServerClosedListener(OnUDPServerClosedListener onUDPServerClosedListener) {
        this.onUDPServerClosedListener = onUDPServerClosedListener;
	}

	public synchronized void startTCPServer() {
        BackGroundTCPServer at = new BackGroundTCPServer();
        at.execute();

        currentTCPServer = at;
	}

	public synchronized void stopTCPServer() {
		synchronized (TCPServerRunning) {
            TCPServerRunning = false;
		}

        try {
            server.close();
        } catch (IOException e) { e.printStackTrace(); }
	}

	public synchronized void startUDPServer() {
        BackGroundUDPServer at = new BackGroundUDPServer();
        at.execute();

        currentUDPServer = at;
	}

	public synchronized void stopUDPServer() {
        Log.i2(this, "Stoping UDP server: " + UDPServerPort);
		synchronized (UDPServerRunning) {
            UDPServerRunning = false;
		}
	}

	private class BackGroundTCPServer {
		protected void execute(Object... params) {
            new Thread() {
                @Override
                public void run() {
                    Log.i2(this, "TCP server started: " + TCPServerPort);
                    TCPServerRunning = true;
                    try {
                        server = new ServerSocket(TCPServerPort);
                        while(TCPServerRunning) {
                            MSocket socket = new MSocket(server.accept());
                            connectionHandler.handle(socket);
                        }
                    } catch (IOException e) { e.printStackTrace(); }


                    if(onTCPServerClosedListener != null)
                        onTCPServerClosedListener.onTCPServerClosed(Server.this);
                }
            }.start();
		}
    }

	private class BackGroundUDPServer {
		protected void execute(Object... params) {
            new Thread() {
                @Override
                public void run() {
                    Log.i2(this, "UDP server started: " + UDPServerPort);
                    UDPServerRunning = true;
                    try {
                        DatagramSocket server = new DatagramSocket(UDPServerPort);
                        server.setBroadcast(true);
                        byte[] receiveData = new byte[1024];

                        while(UDPServerRunning) {
                            DatagramPacket receivePack = new DatagramPacket(receiveData, receiveData.length);
                            server.receive(receivePack);

                            String data = new String(receivePack.getData());
                            connectionHandler.handle(data);
                        }
                        server.close();
                    } catch (IOException e) { e.printStackTrace(); }

                    if(onUDPServerClosedListener != null)
                        onUDPServerClosedListener.onUDPServerClosed(Server.this);
                }
            }.start();
		}
    }

	private class ConnectionHandler {
		public synchronized void handle(final MSocket socket) {
			new Thread() {
				public void run() {
                    INetworkMessage request = null;
                    try {
                        request = socket.getMessage();


                        if(request != null) {
                            INetworkMessage response = handleMessage(request);

                            socket.sendMessage(response);
                            if(onMessageSendedListener != null)
                                onMessageSendedListener.onMessageSent(response.getRequestCode(), response.getMessage());
                        }

                        // *************************
                        socket.closeInOut();
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
				};
			}.start();
		}

		public synchronized void handle(final String request) {
            INetworkMessage msg = NetworkMessageDAO.fromJson(request);
            handleMessage(msg);
		}

		public INetworkMessage handleMessage(INetworkMessage request){
			String s = onMessageReceiveListener != null? onMessageReceiveListener.onMessageReceive(request.getRequestCode(), request.getMessage()): "";
			return new NetworkMessage(request.getRequestCode(), s);
		}

	}
}

