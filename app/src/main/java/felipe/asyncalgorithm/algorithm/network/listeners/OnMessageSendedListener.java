package felipe.asyncalgorithm.algorithm.network.listeners;

/**
 * Created by felipe on 07/11/15.
 */
public interface OnMessageSendedListener {
    void onMessageSent(int requestCode, String message);
}
