package felipe.asyncalgorithm.algorithm.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Vector;

import felipe.asyncalgorithm.algorithm.network.interfaces.INetworkMessage;
import felipe.asyncalgorithm.algorithm.network.listeners.OnClientTCPErrorListener;
import felipe.asyncalgorithm.algorithm.network.listeners.OnClientUDPErrorListener;
import felipe.asyncalgorithm.algorithm.network.listeners.OnMessageReceiveListener;
import felipe.asyncalgorithm.algorithm.network.listeners.OnMessageSendedListener;
import felipe.asyncalgorithm.algorithm.network.utils.NetworkUtils;

public class Client {
	private static Client instance = null;
	
	private List<OnMessageSendedListener> onMessageSendedListener = null;
	private List<OnMessageReceiveListener> onMessageReceiveListener = null;

    private List<OnClientTCPErrorListener> onClientTCPErrorListener = null;
    private List<OnClientUDPErrorListener> onClientUDPErrorListener = null;

	private Client() {
        this.onMessageReceiveListener = new Vector<>();
        this.onMessageSendedListener = new Vector<>();
        this.onClientTCPErrorListener = new Vector<>();
        this.onClientUDPErrorListener = new Vector<>();
    }
	
	public static synchronized Client getInstance() {

		if(instance == null)
			instance = new Client();
		
		return instance;
	}

    public static synchronized Client getNewInstance() {
        return new Client();
    }

	public synchronized void addOnMessageSendedListener(OnMessageSendedListener onMessageSendedListener) {
        synchronized (onMessageSendedListener) {
            this.onMessageSendedListener.add(onMessageSendedListener);
        }
	}

	public synchronized void addOnMessageReceiveListener(OnMessageReceiveListener onMessageReceiveListener) {
        synchronized (onMessageReceiveListener) {
            this.onMessageReceiveListener.add(onMessageReceiveListener);
        }
	}

    public synchronized void addOnClientTCPErrorListener(OnClientTCPErrorListener onClientTCPErrorListener) {
        synchronized (onClientTCPErrorListener) {
            this.onClientTCPErrorListener.add(onClientTCPErrorListener);
        }
    }

    public synchronized void addOnClientUDPErrorListener(OnClientUDPErrorListener onClientUDPErrorListener) {
        synchronized (onClientUDPErrorListener) {
            this.onClientUDPErrorListener.add(onClientUDPErrorListener);
        }
    }

	public synchronized void sendMessage(final int requestCode, final String message, final int serverPort, final String dest) {
        BackgroundTCPClientMessage at = new BackgroundTCPClientMessage(requestCode, message, serverPort, dest);
        at.execute();
	}
	
	public synchronized void sendMessage(final int requestCode, final String message, final int serverPort) {
        BackgroundUDPClientMessage at = new BackgroundUDPClientMessage(requestCode, message, serverPort);
        at.execute();
	}

	private class BackgroundTCPClientMessage  {
        private int requestCode = 0;
        private String message = null;
        private int serverPort = 0;
        private String address = null;

        public BackgroundTCPClientMessage(int requestCode, String message, int serverPort, String address) {
            this.requestCode = requestCode;
            this.message = message;
            this.serverPort = serverPort;
            this.address = address;
        }

        protected Boolean execute(Void... params) {
            new Thread() {
                @Override
                public void run() {
                    INetworkMessage response = null;
                    try {
                        Socket s = new Socket(address, serverPort);
                        //s.setSoTimeout(50 * 1000);
                        MSocket socket = new MSocket(s);

                        INetworkMessage request = new NetworkMessage(requestCode, message);
                        socket.sendMessage(request);

                        for (OnMessageSendedListener l : onMessageSendedListener)
                            l.onMessageSent(requestCode, message);

                        response = socket.getMessage();

                        socket.closeInOut();
                        socket.close();

                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                        for (OnClientTCPErrorListener l : onClientTCPErrorListener)
                            l.onClientTCPError(requestCode, message);

                        return ;
                    } catch (IOException e) {
                        e.printStackTrace();
                        for (OnClientTCPErrorListener l : onClientTCPErrorListener)
                            l.onClientTCPError(requestCode, message);

                        return ;
                    }


                    for (OnMessageReceiveListener l : onMessageReceiveListener) {
                        l.onMessageReceive(response.getRequestCode(), response.getMessage());
                    }


                }
            }.start();

            return true;
        }

    }

    private class BackgroundUDPClientMessage {
        private int requestCode = 0;
        private String message= null;
        private int serverPort = 0;

        public BackgroundUDPClientMessage(int requestCode, String message, int serverPort) {
            this.requestCode = requestCode;
            this.message = message;
            this.serverPort = serverPort;
        }

        protected Boolean execute(Void... params) {
            new Thread() {
                @Override
                public void run() {
                    try {
                        INetworkMessage msg = new NetworkMessage(requestCode, message);
                        String messageData = NetworkMessageDAO.toJson(msg);

                        InetAddress addres = NetworkUtils.getBcastAddress();
                        if(addres != null) {

                            DatagramSocket socket = new DatagramSocket();
                            socket.setBroadcast(true);

                            byte[] sendData = messageData.getBytes();

                            DatagramPacket sendPack = new DatagramPacket(sendData, sendData.length, addres, serverPort);
                            socket.send(sendPack);

                            synchronized (onMessageSendedListener) {
                                for (OnMessageSendedListener l : onMessageSendedListener)
                                    l.onMessageSent(requestCode, message);
                            }

                            socket.close();

                            return ;
                        }
                    } catch (IOException e) { e.printStackTrace(); }

                    synchronized (onClientUDPErrorListener) {
                        for (OnClientUDPErrorListener l : onClientUDPErrorListener)
                            l.onClientUDPError(requestCode, message);
                    }

                }
            }.start();

            return false;
        }
    }

}
