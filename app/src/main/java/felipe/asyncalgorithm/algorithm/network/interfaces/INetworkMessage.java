package felipe.asyncalgorithm.algorithm.network.interfaces;

public interface INetworkMessage {
	int getRequestCode();
	String getMessage();
}
