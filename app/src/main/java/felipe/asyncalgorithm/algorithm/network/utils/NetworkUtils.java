package felipe.asyncalgorithm.algorithm.network.utils;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class NetworkUtils {
	private NetworkUtils() {}
	
	public static InetAddress getBcastAddress() {
		Enumeration<NetworkInterface> interfaces;
		try {
			interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements()) {
				NetworkInterface networkInterface = interfaces.nextElement();
				if (networkInterface.isLoopback())
					continue;    // Don't want to broadcast to the loopback interface
				for (InterfaceAddress interfaceAddress :
					networkInterface.getInterfaceAddresses()) {
					InetAddress broadcast = interfaceAddress.getBroadcast();
					if (broadcast == null)
						continue;
					else 
						return broadcast;
				}
			}
			
		} catch (SocketException e) {e.printStackTrace();}
		
		return null;
	}
	
	public static String getIpAddress() {
	    Enumeration<NetworkInterface> ifaces;
		try {
			ifaces = NetworkInterface.getNetworkInterfaces();
			
			while( ifaces.hasMoreElements() ) {
				NetworkInterface iface = ifaces.nextElement();
				Enumeration<InetAddress> addresses = iface.getInetAddresses();

				while( addresses.hasMoreElements() ) {
					InetAddress addr = addresses.nextElement();
					if( addr instanceof Inet4Address && !addr.isLoopbackAddress() ) {
						return addr.getHostAddress();
					}
				}
			}

		} catch (SocketException e) { e.printStackTrace();}
		
		return null;
	}
}
