package felipe.asyncalgorithm.algorithm.network.listeners;

import felipe.asyncalgorithm.algorithm.network.Server;

/**
 * Created by felipe on 07/11/15.
 */
public interface OnUDPServerClosedListener {
    void onUDPServerClosed(Server server);
}
