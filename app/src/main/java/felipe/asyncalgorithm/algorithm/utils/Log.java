package felipe.asyncalgorithm.algorithm.utils;

import java.util.ArrayList;
import java.util.List;

public class Log {
	private static List<OnLogAddedListener> onLogAddedListeners = new ArrayList<>();

	private Log() {}
	
	public static void i(Object sender, String message) {
        return;
        /*
		android.util.Log.i("DEBUG", sender.getClass().getName() + " : " + message);
		for(OnLogAddedListener l: onLogAddedListeners)
			l.onLogAdded(sender.getClass().getName() + " : " + message);
			*/
	}

	public static void i2(Object sender, String message) {
		android.util.Log.i("DEBUG2", message);
		for(OnLogAddedListener l: onLogAddedListeners)
			l.onLogAdded(sender.getClass().getName() + " : " + message);
	}

	public static void addOnLogAddedListener(OnLogAddedListener onLogAddedListener) {
		Log.onLogAddedListeners.add(onLogAddedListener);
	}

	public interface OnLogAddedListener {
		void onLogAdded(String log);
	}
}
