package felipe.asyncalgorithm.algorithm.model.dao;

import com.google.gson.Gson;

import felipe.asyncalgorithm.algorithm.model.interfaces.IUser;
import felipe.asyncalgorithm.algorithm.model.objects.User;

/**
 * Created by felipe on 07/11/15.
 */
public class UserDAO {

    private UserDAO() {}

    public static String toJson(IUser user) {
        return new Gson().toJson((User)user);
    }

    public static IUser fromJson(String json) {
        try {
            return new Gson().fromJson(json, User.class);
        } catch (Exception e) {}

        return null;
    }
}
