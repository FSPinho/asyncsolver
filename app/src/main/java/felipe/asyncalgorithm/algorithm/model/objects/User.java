package felipe.asyncalgorithm.algorithm.model.objects;

import felipe.asyncalgorithm.algorithm.model.interfaces.IUser;

/**
 * Created by felipe on 07/11/15.
 */
public class User implements IUser {
    private String name;
    private String address;
    private boolean free;

    public User(String name, String address) {
        this.name = name;
        this.address = address;
        this.free = true;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setFree(boolean free) {
        this.free = free;
    }

    @Override
    public boolean isFree() {
        return free;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", free=" + free +
                '}';
    }
}
