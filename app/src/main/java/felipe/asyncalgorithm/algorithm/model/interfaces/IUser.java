package felipe.asyncalgorithm.algorithm.model.interfaces;

/**
 * Created by felipe on 07/11/15.
 */
public interface IUser {
    void setName(String name);
    String getName();

    void setAddress(String address);
    String getAddress();

    void setFree(boolean free);
    boolean isFree();
}
