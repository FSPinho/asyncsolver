package felipe.asyncalgorithm.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import felipe.asyncalgorithm.algorithm.listeners.OnRemoteSolverResultListener;
import felipe.asyncalgorithm.algorithm.listeners.OnSolverIterationListener;
import felipe.asyncalgorithm.algorithm.listeners.OnSolverResultBundleListener;
import felipe.asyncalgorithm.algorithm.networkservice.RemoteSolver;
import felipe.asyncalgorithm.algorithm.utils.Log;

public abstract class BaseAsyncSolver {
    private static Boolean running = false;

    private ObjectId objectId = null;

	Map<Integer, Node> tree = Collections.synchronizedMap(new HashMap<Integer, Node>());

    private Node currentNode = null;
    private Object syncControl = new Object();

	private int maxDeep = 10;
    private int iterationCount = 0;

    public BaseAsyncSolver(int maxDeep) {

        this.maxDeep = maxDeep;

    }

    public static void setRunning(Boolean running) {

        synchronized (running) {

            BaseAsyncSolver.running = running;

        }

    }

    public static Boolean isRunning() {

        return running;

    }

    public void setObjectId(ObjectId objectId) {

        this.objectId = objectId;

    }

    private OnSolverResultBundleListener solverResultBundleListener = null;
    private OnSolverIterationListener solverIterationListener = null;

    public void setSolverResultBundleListener(OnSolverResultBundleListener solverResultBundleListener) {

        synchronized (syncControl) {

            this.solverResultBundleListener = solverResultBundleListener;

        }

    }

    public void setSolverIterationListener(OnSolverIterationListener solverIterationListener) {

        synchronized (syncControl) {

            this.solverIterationListener = solverIterationListener;

        }

    }

    // User
	
	protected abstract void onAlgorithm(Bundle params);
	
	protected abstract Bundle onAlgorithmPreSolved(List<Node> nodes);
	
	protected abstract Bundle onAlgorithmBreakPoint();
	
	protected abstract void onAlgorithmResult(Bundle result);

    public boolean exec(final Node root) {

        if(!running) {

            setRunning(true);

            objectId = new ObjectId(-1);
            tree.clear();
            iterationCount = 0;

            new Thread() {
                @Override
                public void run() {

                    putNode(root);
                    doExecNode(root);

                }

            }.start();

            return true;
        }

        return false;
    }

	public boolean exec(final Bundle params) {

        if(!running) {

            setRunning(true);

            objectId = new ObjectId(-1);
            tree.clear();
            iterationCount = 0;

            new Thread() {
                @Override
                public void run() {

                    Node root = new Node(-1, params, objectId);
                    putNode(root);
                    doExecNode(root);

                }

            }.start();

            return true;
        }

        return false;

	}
	
	// Background
	
	private void doExecNode(Node node) {

        synchronized (syncControl) {

            iterationCount++;
            if (solverIterationListener != null) {

                solverIterationListener.onSolverIteration(iterationCount, node.getLevel());

            }

            currentNode = node;
            onAlgorithm(node.getDataParams());

        }

        doExecNodes(node);

	}
	
	protected void doExecAgain(Bundle dataParams) {

        if(currentNode.getLevel() < maxDeep) {

            Node child = currentNode.createChild(dataParams, objectId);
            putNode(child);

        }

	}
	
	private void doExecNodes(Node node) {

        List<Node> children = getNodeChildren(node);

        if (children.size() > 0) {

            for (final Node n : children) {

                if(n.getLevel() < 5) {

                    RemoteSolver remoteSolver = new RemoteSolver();
                    remoteSolver.setRemoteSolverResultListener(new OnRemoteSolverResultListener() {
                        @Override
                        public void onRemoteResult(Node rNode) {

                            if (!n.equals(rNode)) {

                                Log.i2(this, "Algorithm error!");

                            }

                            putNode(rNode);
                            onNodeResult(rNode);

                        }

                        @Override
                        public void onRemoteResultError(Node rNode) {

                            if (!n.equals(rNode)) {

                                Log.i2(this, "Algorithm error!");

                            }

                            putNode(rNode);
                            doExecNode(rNode);

                        }

                    });

                    remoteSolver.solveRemote(n);

                } else {
                    doExecNode(n);
                }
            }

        } else {

            onNodeBreakResult(node);

        }

	}
	
	private void onNodeResult(Node node) {

        synchronized (syncControl) {

            Node parent = getNode(node.getParentId());

            if (parent != null) {

                if (!parent.containsChild(node)) {

                    Log.i2(this, "Algorithm error!");

                }

                parent.addCompleteChild();

                if (parent.getChildrenId().size() - parent.getChildrenCompleteCount() < 0) {

                    Log.i2(this, "Algorithm error!");

                }

                if (isChildrenComplete(parent)) {

                    Bundle dataResult = onAlgorithmPreSolved(getNodeChildren(parent));
                    parent.setDataResult(dataResult);
                    onNodeResult(parent);

                }

            }

            // Root node
            else {

                Log.i2(this, "Finished on root!");

                synchronized (running) {

                    running = false;

                }

                onAlgorithmResult(node.getDataResult());

                if (solverResultBundleListener != null) {

                    solverResultBundleListener.onSolverResult(node.getDataResult(), iterationCount);

                }

            }

        }

	}
	
	private void onNodeBreakResult(Node node) {

        synchronized (syncControl) {

            Bundle dataResult = onAlgorithmBreakPoint();
            node.setDataResult(dataResult);

            onNodeResult(node);

        }

	}
	
	// Auxiliary
	private List<Node> getNodeChildren(Node node) {

		List<Node> children = new ArrayList<>();
		for(Integer id: node.getChildrenId()) {

            children.add(getNode(id));

        }
		
		return children;

	}
	
	private boolean isChildrenComplete(Node node) {

        return node.isComplete();

	}

    private void putNode(Node node) {

        synchronized (syncControl) {

            Integer key = new Integer(node.getId());
            tree.put(key, node);

        }

    }

    private Node getNode(Integer key) {

        Integer k = new Integer(key);
        return tree.get(k);

    }

    public void showTreeInfo() {
        synchronized (syncControl) {

            List<Node> nodes = new Vector<>();
            nodes.addAll(tree.values());

            int finishedCount = 0;
            for (Node n : nodes) {

                if (n.isComplete()) {

                    finishedCount++;

                }

            }

            Log.i2(this, "Tree info: " + finishedCount + " nodes finished of " + nodes.size());

        }

    }

}
