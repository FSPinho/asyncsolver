package felipe.asyncalgorithm.algorithm;

/**
 * Created by felipe on 05/12/15.
 */
public class ObjectId {
    private Integer ID = -1;

    public ObjectId() { }

    public ObjectId(Integer ID) {
        this.ID = ID;
    }

    public synchronized Integer getNewId() {
        synchronized (ID) {
            ID = ID.intValue() + 1;
        }
        return ID.intValue();
    }

    public synchronized void setID(int id) {
        synchronized (ID) {
            ID = id;
        }
    }
}
