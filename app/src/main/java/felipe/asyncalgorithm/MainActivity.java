package felipe.asyncalgorithm;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.List;

import felipe.asyncalgorithm.algorithm.BaseAsyncSolver;
import felipe.asyncalgorithm.algorithm.listeners.ISolverHelperListener;
import felipe.asyncalgorithm.algorithm.listeners.OnSolverIterationListener;
import felipe.asyncalgorithm.algorithm.listeners.OnSolverResultBundleListener;
import felipe.asyncalgorithm.algorithm.networkservice.SolverHelper;
import felipe.asyncalgorithm.algorithm.networkservice.UsersFinder;
import felipe.asyncalgorithm.algorithm.networkservice.listeners.OnUsersUpdateListener;
import felipe.asyncalgorithm.algorithm.model.interfaces.IUser;

public class MainActivity extends AppCompatActivity {
    EditText etN;
    TextView tvLog;
    TextView tvUsers;
    Button btnCalculate;
    Button btnTreeInfo;
    ToggleButton btnAllowMyself;

    final UsersFinder finder = UsersFinder.getInstance();

    private BaseAsyncSolver currentSolver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etN = (EditText) findViewById(R.id.etN);
        tvLog = (TextView) findViewById(R.id.tvLog);
        tvUsers = (TextView) findViewById(R.id.tvUsers);
        btnCalculate = (Button) findViewById(R.id.btnCalculate);
        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = etN.getText().toString();
                calculate(Integer.parseInt(s));
            }
        });

        btnAllowMyself = (ToggleButton) findViewById(R.id.btnAllowMyself);
        btnAllowMyself.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                finder.setAllowMyselfAsUser(isChecked);
            }
        });

        btnTreeInfo = (Button) findViewById(R.id.btnTreeInfo);
        btnTreeInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentSolver != null)
                    currentSolver.showTreeInfo();
            }
        });

        finder.addOnUsersUpdateListener(new OnUsersUpdateListener() {
            @Override
            public void onUsersUpdate(final List<IUser> users) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String text = "";
                        for (IUser u : users) {
                            text += u.getName() + "\n";
                        }
                        tvUsers.setText(text);
                    }
                });
            }
        });

        SolverHelper helper = new SolverHelper();
        helper.setSolver(new FibonacciSolver(100));
        helper.start();

        helper.setListener(new ISolverHelperListener() {
            @Override
            public void onSolverStart() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvLog.setText("Ajudando outro dispositivo");
                    }
                });

            }

            @Override
            public void onRecussionCalled(final int itarationCount, final int deep) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvLog.setText("Ajudando outro dispositivo: " + itarationCount + " iterations and deep " + deep);
                    }
                });

            }

            @Override
            public void onSolverFinish(final long timeElapsed, final int iterationCount) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvLog.setText("Ajuda finalizada: " + iterationCount + " iterations");
                    }
                });

            }
        });
    }

    public void calculate(int n) {
        FibonacciSolver solver = new FibonacciSolver(100);
        solver.setSolverResultBundleListener(new OnSolverResultBundleListener() {
            @Override
            public void onSolverResult(final felipe.asyncalgorithm.algorithm.Bundle result, int iterationCount) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvLog.setText("O resultado é " + result.getInt("R"));
                    }
                });
            }
        });
        solver.setSolverIterationListener(new OnSolverIterationListener() {
            @Override
            public void onSolverIteration(final int iterationCount, final int deep) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvLog.setText("Resolvendo movimento: " + iterationCount + " and deep " + deep);
                    }
                });

            }
        });

        felipe.asyncalgorithm.algorithm.Bundle params = new felipe.asyncalgorithm.algorithm.Bundle();
        params.put("N", n);

        if(solver.exec(params)) {
            Toast.makeText(MainActivity.this, "Exec fibonacci of " + n, Toast.LENGTH_SHORT).show();
            currentSolver = solver;
        } else {
            Toast.makeText(MainActivity.this, "Can't exec, solver already in use!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onDestroy() {

        finder.stopSearch();
        super.onDestroy();

    }
}
