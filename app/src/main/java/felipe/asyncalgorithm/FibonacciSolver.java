package felipe.asyncalgorithm;

import java.util.List;

import felipe.asyncalgorithm.algorithm.BaseAsyncSolver;
import felipe.asyncalgorithm.algorithm.Node;

/**
 * Created by felipe on 05/12/15.
 */
public class FibonacciSolver extends BaseAsyncSolver {

    public FibonacciSolver(int maxDeep) {
        super(maxDeep);
    }

    @Override
    protected void onAlgorithm(felipe.asyncalgorithm.algorithm.Bundle params) {
        int n = params.getInt("N");
        if(n > 1) {
            felipe.asyncalgorithm.algorithm.Bundle p1 = new felipe.asyncalgorithm.algorithm.Bundle();
            p1.put("N", n - 1);
            doExecAgain(p1);

            felipe.asyncalgorithm.algorithm.Bundle p2 = new felipe.asyncalgorithm.algorithm.Bundle();
            p2.put("N", n - 2);
            doExecAgain(p2);
        }
    }

    @Override
    protected felipe.asyncalgorithm.algorithm.Bundle onAlgorithmPreSolved(List<Node> nodes) {
        felipe.asyncalgorithm.algorithm.Bundle result = new felipe.asyncalgorithm.algorithm.Bundle();
        result.put("R", nodes.get(0).getDataResult().getInt("R") + nodes.get(1).getDataResult().getInt("R"));
        return result;
    }

    @Override
    protected felipe.asyncalgorithm.algorithm.Bundle onAlgorithmBreakPoint() {
        felipe.asyncalgorithm.algorithm.Bundle result = new felipe.asyncalgorithm.algorithm.Bundle();
        result.put("R", 1);
        return result;
    }

    @Override
    protected void onAlgorithmResult(final felipe.asyncalgorithm.algorithm.Bundle result) {

    }
}
